package kusmierz.wlodzimierz.pokemon.evolution;

import java.util.List;

import kusmierz.wlodzimierz.pokemon.evolution.Models.Pokemon;
import kusmierz.wlodzimierz.pokemon.evolution.Models.PokemonAfterCalculations;


public class PokemonApplication extends android.app.Application {

    private List<Pokemon> pokemonList;
    private Pokemon chosenPokemon;
    private List<PokemonAfterCalculations> pokemonListAfterCalculations;

    public List<PokemonAfterCalculations> getPokemonListAfterCalculations() {
        return pokemonListAfterCalculations;
    }

    public void setPokemonListAfterCalculations(List<PokemonAfterCalculations> pokemonListAfterCalculations) {
        this.pokemonListAfterCalculations = pokemonListAfterCalculations;
    }

    public Pokemon getChosenPokemon() {
        return chosenPokemon;
    }

    public void setChosenPokemon(Pokemon chosenPokemon) {
        this.chosenPokemon = chosenPokemon;
    }

    public List<Pokemon> getPokemonList() {
        return pokemonList;
    }

    public void setPokemonList(List<Pokemon> pokemonList) {
        this.pokemonList = pokemonList;
    }
}

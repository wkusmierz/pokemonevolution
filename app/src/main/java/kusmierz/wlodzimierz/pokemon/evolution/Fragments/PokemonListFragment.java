package kusmierz.wlodzimierz.pokemon.evolution.Fragments;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import kusmierz.wlodzimierz.pokemon.evolution.Activities.MainActivity;
import kusmierz.wlodzimierz.pokemon.evolution.Adapters.PokemonListAdapter;
import kusmierz.wlodzimierz.pokemon.evolution.Models.Pokemon;
import kusmierz.wlodzimierz.pokemon.evolution.PokemonApplication;
import kusmierz.wlodzimierz.pokemon.evolution.R;
import kusmierz.wlodzimierz.pokemon.evolution.Utils.PokemonComparator;


public class PokemonListFragment extends Fragment {

    View rootView;
    MainActivity mainActivity = null;
    private PokemonListAdapter adapter = null;
    @BindView(R.id.listView_fragment_users_list)
    ListView listView;
    @BindView(R.id.editText_fragment_pokemon_list)
    EditText inputSearch;
    @BindDrawable(R.drawable.ic_search_white_24dp)
    Drawable searchIcon;



    public PokemonListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_pokemon_list, container, false);
        //setHasOptionsMenu(true);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        setPokemonList();

        mainActivity.startAnim();

        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {

                Pokemon pokemon;
                pokemon = dataSnapshot.getValue(Pokemon.class);

                addPokemonToList(pokemon);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        if(((PokemonApplication) getActivity().getApplication()).getPokemonList().size()==0)
        {
            mainActivity.getmDatabase().child("pokemon").addChildEventListener(childEventListener);
        }


        inputSearch.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    adapter.getFilter().filter(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {


                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        inputSearch.setCompoundDrawablesWithIntrinsicBounds(searchIcon, null, null, null);
    }

    private void addPokemonToList(Pokemon pokemon)
    {
        ((PokemonApplication) getActivity().getApplication()).getPokemonList().add(pokemon);

        Collections.sort(((PokemonApplication) getActivity().getApplication()).getPokemonList(), PokemonComparator.ALPHABETICAL_ORDER);

        Log.d("Name ", pokemon.getName());
        mainActivity.stopAnim();

        adapter.notifyDataSetChanged();
    }

    private void setPokemonList()
    {
        if(((PokemonApplication) getActivity().getApplication()).getPokemonList() == null)
        {
            ((PokemonApplication) getActivity().getApplication()).setPokemonList(new ArrayList<Pokemon>());
        }

        adapter = new PokemonListAdapter(getActivity(),((PokemonApplication) getActivity().getApplication()).getPokemonList());
        listView.setAdapter(adapter);
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (MainActivity) activity;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {


        super.onSaveInstanceState(outState);
    }

}

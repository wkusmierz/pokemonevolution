package kusmierz.wlodzimierz.pokemon.evolution.Utils;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kusmierz.wlodzimierz.pokemon.evolution.Models.Pokemon;
import kusmierz.wlodzimierz.pokemon.evolution.Models.PokemonAfterCalculations;

/**
 * Created by admin on 2016-07-23.
 */
public class Calculations {

    public static List<PokemonAfterCalculations> getCPs(Pokemon pokemon, int cp)
    {
        List<PokemonAfterCalculations> list = new ArrayList<>();
        //string+=pokemon.getName() + " Evolutions: ";
        list = recurrence(list,pokemon,cp);
        return list;
    }

    private static List<PokemonAfterCalculations> recurrence(List<PokemonAfterCalculations> list, Pokemon pokemon, int cp)
    {
        float evoModifier;
        PokemonAfterCalculations pokemonAfterCalculations;

        if(pokemon.getEvolutions()==null)
        {
            return list;
        }
        else if(pokemon.getEvolutions() != null && pokemon.getEvoModifier() == null)
        {
            for(int i=0;i<pokemon.getEvolutions().size();i++)
            {
                evoModifier = Float.parseFloat(pokemon.getEvolutions().get(i).getEvoModifier());

                pokemonAfterCalculations = new PokemonAfterCalculations(pokemon.getEvolutions().get(i).getName(),pokemon.getEvolutions().get(i).getImage(),
                        (int)(cp*evoModifier));

                list.add(pokemonAfterCalculations);
            }
            return list;

        }
        else
        {
            evoModifier = Float.parseFloat(pokemon.getEvoModifier());
            cp*=evoModifier;

            Log.d("Float parsed ", String.valueOf(evoModifier));

            pokemonAfterCalculations = new PokemonAfterCalculations(pokemon.getEvolutions().get(0).getName(),pokemon.getEvolutions().get(0).getImage(),
                    cp);

            list.add(pokemonAfterCalculations);

            return recurrence(list, pokemon.getEvolutions().get(0),cp);
        }
    }

    public static void alertDialog(Context context)
    {

    }
}

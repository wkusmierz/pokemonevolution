package kusmierz.wlodzimierz.pokemon.evolution.Utils;

import java.util.Comparator;

import kusmierz.wlodzimierz.pokemon.evolution.Models.Pokemon;

public class PokemonComparator {

    public static Comparator<Pokemon> ALPHABETICAL_ORDER = new Comparator<Pokemon>() {
        public int compare(Pokemon str1, Pokemon str2) {
            int res = String.CASE_INSENSITIVE_ORDER.compare(str1.getName(), str2.getName());
            if (res == 0) {
                res = str1.getName().compareTo(str2.getName());
            }
            return res;
        }
    };
}



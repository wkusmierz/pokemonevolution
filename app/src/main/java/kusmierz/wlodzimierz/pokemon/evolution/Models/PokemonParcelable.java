package kusmierz.wlodzimierz.pokemon.evolution.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class PokemonParcelable implements Parcelable {

    private List<Pokemon> listOfPokemons;
    private Pokemon pokemonChosen;

    public PokemonParcelable(List<Pokemon> listOfPokemons, Pokemon pokemonChosen) {
        this.listOfPokemons = listOfPokemons;
        this.pokemonChosen = pokemonChosen;
    }

    public List<Pokemon> getListOfPokemons() {

        return listOfPokemons;
    }

    public void setListOfPokemons(List<Pokemon> listOfPokemons) {
        this.listOfPokemons = listOfPokemons;
    }

    public Pokemon getPokemonChosen() {
        return pokemonChosen;
    }

    public void setPokemonChosen(Pokemon pokemonChosen) {
        this.pokemonChosen = pokemonChosen;
    }

    protected PokemonParcelable(Parcel in) {
        if (in.readByte() == 0x01) {
            listOfPokemons = new ArrayList<Pokemon>();
            in.readList(listOfPokemons, Pokemon.class.getClassLoader());
        } else {
            listOfPokemons = null;
        }
        pokemonChosen = (Pokemon) in.readValue(Pokemon.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (listOfPokemons == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(listOfPokemons);
        }
        dest.writeValue(pokemonChosen);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PokemonParcelable> CREATOR = new Parcelable.Creator<PokemonParcelable>() {
        @Override
        public PokemonParcelable createFromParcel(Parcel in) {
            return new PokemonParcelable(in);
        }

        @Override
        public PokemonParcelable[] newArray(int size) {
            return new PokemonParcelable[size];
        }
    };
}
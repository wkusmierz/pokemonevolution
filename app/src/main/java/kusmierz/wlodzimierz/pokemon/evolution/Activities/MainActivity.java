package kusmierz.wlodzimierz.pokemon.evolution.Activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kusmierz.wlodzimierz.pokemon.evolution.Fragments.PokemonFragment;
import kusmierz.wlodzimierz.pokemon.evolution.Fragments.PokemonListFragment;
import kusmierz.wlodzimierz.pokemon.evolution.Models.Pokemon;
import kusmierz.wlodzimierz.pokemon.evolution.Models.PokemonParcelable;
import kusmierz.wlodzimierz.pokemon.evolution.PokemonApplication;
import kusmierz.wlodzimierz.pokemon.evolution.R;
import kusmierz.wlodzimierz.pokemon.evolution.Utils.SessionIdentifierGenerator;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private Fragment fragment = null;
    private FragmentManager fragmentManager = null;
    List<Pokemon> list;
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {

            PokemonParcelable pokemonRetrieved = savedInstanceState.getParcelable("savedData");
            ((PokemonApplication) getApplication()).setPokemonList(pokemonRetrieved.getListOfPokemons());
            ((PokemonApplication) getApplication()).setChosenPokemon(pokemonRetrieved.getPokemonChosen());

        }

        list = new ArrayList<>();
        ButterKnife.bind(this);
        myToolbar.setTitleTextColor(0xFFFFFFFF);


        mDatabase = FirebaseDatabase.getInstance().getReference();


        fragment = new PokemonListFragment();
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainActivityLayout, fragment);
        fragmentTransaction.commit();

    }

    public void listElementOnClick()
    {
        fragment = new PokemonFragment();
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainActivityLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public DatabaseReference getmDatabase() {
        return mDatabase;
    }

    private void writeNewTest(String id, String test) {

        SessionIdentifierGenerator generator = new SessionIdentifierGenerator();


    }

    @OnClick(R.id.imageView_backButton_Toolbar)
    public void back(View view) {
       onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //    finish();

    }

    public void startAnim(){

        if(((PokemonApplication) getApplication()).getPokemonList().size()==0)
        {
            findViewById(R.id.avloadingIndicatorView).setVisibility(View.VISIBLE);
        }

    }

    public void stopAnim(){
        findViewById(R.id.avloadingIndicatorView).setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        super.onSaveInstanceState(savedInstanceState);

        PokemonParcelable pokemon = new PokemonParcelable(((PokemonApplication) getApplication()).getPokemonList(), ((PokemonApplication) getApplication()).getChosenPokemon());
        savedInstanceState.putParcelable("savedData", pokemon);
    }





}

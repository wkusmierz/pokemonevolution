package kusmierz.wlodzimierz.pokemon.evolution.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Pokemon implements Parcelable {


    private String name;
    private String evoModifier;
    private List<Pokemon> evolutions;
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Pokemon(){}

    public Pokemon(String name, String evoModifier, List<Pokemon> evolutions, String image){

        this.name = name;
        this.evoModifier = evoModifier;
        this.evolutions = evolutions;
        this.image = image;
    }

    public Pokemon(String name){

        this.name = name;
    }

    public Pokemon(String name, String image)
    {
        this.name = name;
        this.image = image;
    }

    public Pokemon(String name, String evoModifier, Pokemon singleEvo, String image)
    {
        this.name = name;
        this.evoModifier = evoModifier;
        this.image = image;

        List<Pokemon> evo = new ArrayList<>();
        evo.add(singleEvo);
        evolutions = evo;
    }


    public Pokemon(String name, List<Pokemon> evolutions, String image)
    {
        this.name = name;
        this.evolutions = evolutions;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEvoModifier() {
        return evoModifier;
    }

    public void setEvoModifier(String evoModifier) {
        this.evoModifier = evoModifier;
    }

    public List<Pokemon> getEvolutions() {
        return evolutions;
    }

    public void setEvolutions(List<Pokemon> evolutions) {
        this.evolutions = evolutions;
    }

    protected Pokemon(Parcel in) {
        name = in.readString();
        evoModifier = in.readString();
        if (in.readByte() == 0x01) {
            evolutions = new ArrayList<Pokemon>();
            in.readList(evolutions, Pokemon.class.getClassLoader());
        } else {
            evolutions = null;
        }
        image = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(evoModifier);
        if (evolutions == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(evolutions);
        }
        dest.writeString(image);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Pokemon> CREATOR = new Parcelable.Creator<Pokemon>() {
        @Override
        public Pokemon createFromParcel(Parcel in) {
            return new Pokemon(in);
        }

        @Override
        public Pokemon[] newArray(int size) {
            return new Pokemon[size];
        }
    };
}
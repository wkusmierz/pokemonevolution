package kusmierz.wlodzimierz.pokemon.evolution.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kusmierz.wlodzimierz.pokemon.evolution.Activities.MainActivity;
import kusmierz.wlodzimierz.pokemon.evolution.Adapters.PokemonEvolutionsAdapter;
import kusmierz.wlodzimierz.pokemon.evolution.PokemonApplication;
import kusmierz.wlodzimierz.pokemon.evolution.R;
import kusmierz.wlodzimierz.pokemon.evolution.Utils.Calculations;

public class PokemonFragment extends Fragment {

    private View rootView;
    private MainActivity mainActivity = null;
    private PokemonEvolutionsAdapter adapter = null;
    private AlertDialog.Builder alert;


    @BindView(R.id.listView_fragment_chosen_pokemon)
    ListView listView;
    @BindView(R.id.imageView_pokemon_chosen_pokemon_chosen_image)
    ImageView imageViewMainPokemon;
    @BindView(R.id.textView_fragment_pokemon_chosen_pokemon_chosen)
    TextView textViewMainPokemon;
    @BindString(R.string.alert_title)
    String alertTitle;



    public PokemonFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_pokemon, container, false);

        ButterKnife.bind(this,rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((PokemonApplication) getActivity().getApplication()).setPokemonListAfterCalculations(Calculations.getCPs(((PokemonApplication) getActivity().getApplication()).getChosenPokemon(),0));
        setAdapter();

        Picasso.with(rootView.getContext()).load(((PokemonApplication) getActivity().getApplication()).getChosenPokemon().getImage()).into(imageViewMainPokemon);
        textViewMainPokemon.setText(((PokemonApplication) getActivity().getApplication()).getChosenPokemon().getName());

    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (MainActivity) activity;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {


        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.buttonScan)
    public void buttonEvolve(View view) {
        showAlert();
    }

    private void showAlert()
    {
        alert = new AlertDialog.Builder(rootView.getContext());

        final EditText edittext = new EditText(rootView.getContext());
        edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
        alert.setTitle(alertTitle);

        alert.setView(edittext);

        alert.setPositiveButton("Calculate", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                String YouEditTextValue = edittext.getText().toString();

                Log.d("string ", edittext.getText().toString());

                ((PokemonApplication) getActivity().getApplication()).setPokemonListAfterCalculations(Calculations.getCPs(((PokemonApplication) getActivity().getApplication()).getChosenPokemon(),Integer.parseInt(YouEditTextValue)));
                setAdapter();
            }
        });

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }

    private void setAdapter()
    {
        adapter = new PokemonEvolutionsAdapter(getActivity(),((PokemonApplication) getActivity().getApplication()).getPokemonListAfterCalculations());
        listView.setAdapter(adapter);
    }

}
package kusmierz.wlodzimierz.pokemon.evolution.Adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kusmierz.wlodzimierz.pokemon.evolution.Activities.MainActivity;
import kusmierz.wlodzimierz.pokemon.evolution.Models.Pokemon;
import kusmierz.wlodzimierz.pokemon.evolution.PokemonApplication;
import kusmierz.wlodzimierz.pokemon.evolution.R;

public class PokemonListAdapter extends BaseAdapter implements View.OnClickListener {

    private Activity activity;
    private List<Pokemon> data;
    CustomFilter planetFilter;
    private static LayoutInflater inflater = null;
    List<Pokemon> list2;

    public PokemonListAdapter(Activity a, List<Pokemon> d) {

        activity = a;
        data = d;
        list2 = d;
        inflater = (LayoutInflater) activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder {

        @BindView(R.id.textView_user_list_element)
        TextView name;
        @BindView(R.id.imageView_user_list_element)
        ImageView avatar;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }


    }

    public View getView(final int position, View convertView, final ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder=null;


        if (convertView == null) {

            vi = inflater.inflate(R.layout.pokemon_list_element, null);
            holder = new ViewHolder(vi);
            vi.setTag(holder);

        } else
            holder = (ViewHolder) vi.getTag();



        holder.name.setText(data.get(position).getName());
        if(data.get(position).getImage() != null)
        {
            Picasso.with(vi.getContext()).load(data.get(position).getImage()).into(holder.avatar);
        }
        else
        {
            Picasso.with(vi.getContext()).load("http://cdn.bulbagarden.net/upload/thumb/7/78/150Mewtwo.png/250px-150Mewtwo.png").into(holder.avatar);

        }

        vi.setOnClickListener(new OnItemClickListener(position));
        return vi;
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub

    }



    private class OnItemClickListener implements View.OnClickListener {
        private int mPosition;
        volatile int i = 0;


        OnItemClickListener(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {


            MainActivity mainActivity = (MainActivity) activity;

            Log.d("POKEMON, ",data.get(mPosition).getName());
            ((PokemonApplication) mainActivity.getApplication()).setChosenPokemon(data.get(mPosition));
            mainActivity.listElementOnClick();



        }
    }

    public Filter getFilter() {
        if (planetFilter == null)
            planetFilter = new CustomFilter();

        return planetFilter;
    }


    public class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                results.values = list2;
                results.count = list2.size();
            }

            else {
                List<Pokemon> nPlanetList = new ArrayList<>();
                data=list2;

                for (Pokemon p : data) {
                    if (p.getName().toUpperCase().startsWith(constraint.toString().toUpperCase()))
                        nPlanetList.add(p);
                }

                results.values = nPlanetList;
                results.count = nPlanetList.size();

            }
            return results;
        }

        @Override

        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count == 0)
                notifyDataSetChanged();
            else {
                data = (List<Pokemon>) results.values;
                notifyDataSetChanged();
            }
        }
    }

}
package kusmierz.wlodzimierz.pokemon.evolution.Models;


import android.os.Parcel;
import android.os.Parcelable;

public class PokemonAfterCalculations implements Parcelable {

    private String name;
    private String image;
    private int cp;

    public PokemonAfterCalculations(String name, String image, int cp) {
        this.name = name;
        this.image = image;
        this.cp = cp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    protected PokemonAfterCalculations(Parcel in) {
        name = in.readString();
        image = in.readString();
        cp = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(image);
        dest.writeInt(cp);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PokemonAfterCalculations> CREATOR = new Parcelable.Creator<PokemonAfterCalculations>() {
        @Override
        public PokemonAfterCalculations createFromParcel(Parcel in) {
            return new PokemonAfterCalculations(in);
        }

        @Override
        public PokemonAfterCalculations[] newArray(int size) {
            return new PokemonAfterCalculations[size];
        }
    };
}
package kusmierz.wlodzimierz.pokemon.evolution.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kusmierz.wlodzimierz.pokemon.evolution.Models.PokemonAfterCalculations;
import kusmierz.wlodzimierz.pokemon.evolution.R;


public class PokemonEvolutionsAdapter extends BaseAdapter {

    private Activity activity;
    private List<PokemonAfterCalculations> data;
    private static LayoutInflater inflater = null;

    public PokemonEvolutionsAdapter(Activity a, List<PokemonAfterCalculations> d) {

        activity = a;
        data = d;
        inflater = (LayoutInflater) activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder {

        @BindView(R.id.textView_user_list_element)
        TextView name;
        @BindView(R.id.imageView_user_list_element)
        ImageView avatar;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }


    }

    public View getView(final int position, View convertView, final ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder=null;


        if (convertView == null) {

            vi = inflater.inflate(R.layout.pokemon_list_element, null);
            holder = new ViewHolder(vi);
            vi.setTag(holder);

        } else
            holder = (ViewHolder) vi.getTag();


        if(data.get(position).getCp()==0)
        {
            holder.name.setText(data.get(position).getName());
        }
        else
        {
            holder.name.setText(data.get(position).getName() + " - "+ data.get(position).getCp()+" CP");
        }

        if(data.get(position).getImage() != null)
        {
            Picasso.with(vi.getContext()).load(data.get(position).getImage()).into(holder.avatar);
        }
        else
        {

            Picasso.with(vi.getContext()).load("http://cdn.bulbagarden.net/upload/thumb/7/78/150Mewtwo.png/250px-150Mewtwo.png").into(holder.avatar);

        }






        return vi;
    }







}